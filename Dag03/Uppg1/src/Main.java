import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";
        ArrayList<String> inputList = new ArrayList<>();
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                inputList.add(rad);
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}

        //Deklarera variabler
        int inputSize = inputList.size();
        int partNumberAddition = 0;
        String previousLine = "None";
        String nextLine = "None";

        //Gå igenom alla rader och kolla om nummer har närliggande tecken
        for (int i = 0; i < inputSize; i++) {
            String currLine = inputList.get(i);
            int lineIdx = 0;
            if (i != 0) {
                previousLine = inputList.get(i-1);
            }
            if (i < inputSize-2) {
                nextLine = inputList.get(i+1);
            }

            //hitta mönster av nummer
            Pattern pattern = Pattern.compile("\\b\\d+\\b");
            Matcher m = pattern.matcher(currLine);

            //Gå igenom alla nummer på en rad
            while (m.find()) {
                String number = m.group(0);

                int idxFirst = currLine.indexOf(number, lineIdx);
                int idxLast = idxFirst+number.length()-1;
                boolean hasOtherCharsAbove = false;
                boolean hasOtherCharsBelow = false;
                boolean hasOtherCharsSide = false;

                //Kika på raden ovanför
                if (!Objects.equals(previousLine, "None"))  {
                    String substr = '.'+previousLine+'.';
                    substr = substr.substring(idxFirst, idxLast+3);
                    hasOtherCharsAbove = !substr.matches("^[.0-9]+$");
                }

                //Kika på raden nedanför
                if (!Objects.equals(nextLine, "None"))  {
                    String substr = '.'+nextLine+'.';
                    substr = substr.substring(idxFirst, idxLast+3);
                    hasOtherCharsBelow = !substr.matches("^[.0-9]+$");
                }

                //Kika till höger och vänster om numret
                String substr = '.'+inputList.get(i)+'.';
                substr = substr.substring(idxFirst, idxLast+3);
                if ((substr.charAt(0) != '.') || (substr.charAt(substr.length()-1) != '.')) {
                    hasOtherCharsSide = true;
                }

                if (hasOtherCharsAbove || hasOtherCharsBelow || hasOtherCharsSide ) {
                    partNumberAddition =  partNumberAddition + Integer.parseInt(number);
                }

                //Liten räddning för att upptäcka om samma nummer förekommer flera ggr på en rad
                lineIdx = idxLast;
            }

        }
        System.out.println(partNumberAddition);
    }
}