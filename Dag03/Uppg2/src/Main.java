import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";
        ArrayList<String> inputList = new ArrayList<>();
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                inputList.add(rad);
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}

        //Deklarera variabler
        int inputSize = inputList.size();
        List<List<List<Integer>>> starChart = new ArrayList<>();
        int numRows = inputList.size();
        int numCols = inputList.get(0).length();

        //Skapa matris med tomma listor
        for (int i = 0; i < numRows; i++) {
            List<List<Integer>> row = new ArrayList<>();
            for (int j = 0; j < numCols; j++) {
                row.add(new ArrayList<>());
            }
            starChart.add(row);
        }

        int gearNumberAddition = 0;


        //Gå igenom alla rader och kolla efter nummer och stjärnor
        for (int i = 0; i < inputSize; i++) {
            String previousLine = "None";
            String nextLine = "None";
            String currLine = inputList.get(i);
            int lineIdx = 0;
            if (i != 0) {
                previousLine = inputList.get(i-1);
            }
            if (i < inputSize-2) {
                nextLine = inputList.get(i+1);
            }

            //Hitta mönster av nummer
            Pattern pattern = Pattern.compile("\\b\\d+\\b");
            Matcher m = pattern.matcher(currLine);

            //Gå igenom alla nummer på en rad
            while (m.find()) {
                String number = m.group(0);

                int idxFirst = currLine.indexOf(number, lineIdx);
                int idxLast = idxFirst+number.length()-1;

                //Kika på raden ovanför
                if (!Objects.equals(previousLine, "None"))  {
                    String substr = '.'+previousLine+'.';
                    substr = substr.substring(idxFirst, idxLast+3);
                    ArrayList<Integer> idxesList = getStars(substr);
                    for (int j = 0; j < idxesList.size(); j++) {
                        starChart.get(i-1).get(idxesList.get(j)+idxFirst).add(Integer.parseInt(number));
                    }
                }

                //Kika på raden nedanför
                if (!Objects.equals(nextLine, "None"))  {
                    String substr = '.'+nextLine+'.';
                    substr = substr.substring(idxFirst, idxLast+3);
                    ArrayList<Integer> idxesList = getStars(substr);
                    for (int j = 0; j < idxesList.size(); j++) {
                        starChart.get(i+1).get(idxesList.get(j)+idxFirst).add(Integer.parseInt(number));
                    }
                }

                //Kika till höger och vänster om numret
                String substr = '.'+inputList.get(i)+'.';
                substr = substr.substring(idxFirst, idxLast+3);
                if (substr.charAt(0) == '*') {
                    starChart.get(i).get(idxFirst-1).add(Integer.parseInt(number));
                }
                if (substr.charAt(substr.length()-1) == '*') {
                    starChart.get(i).get(idxLast+1).add(Integer.parseInt(number));
                }

                //Liten räddning för att upptäcka om samma nummer förekommer flera ggr på en rad
                lineIdx = idxLast;
            }

        }

        //Hämta ut gear-numret
        for (List<List<Integer>> row : starChart) {
            for (List<Integer> list : row) {
                if (list.size() == 2){
                    gearNumberAddition = gearNumberAddition + (list.get(0)*list.get(1));
                }
            }
        }

        System.out.println("Adderad gear: " + gearNumberAddition);

    }

    // Hjälpfunktion som tar fram index för stjärnor
    private static ArrayList<Integer> getStars(String subline) {
        ArrayList<Integer> idxesList = new ArrayList<>();

        Pattern p = Pattern.compile("\\*");
        Matcher m = p.matcher(subline);

        while(m.find()) {
            idxesList.add(m.start()-1);
        }
        return idxesList;
    }

}