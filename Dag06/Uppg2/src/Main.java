import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";

        //Deklarera listor att lägga input i
        List<List<Long>> arr = new ArrayList<>();
        arr.add(new ArrayList<>());  // Times
        arr.add(new ArrayList<>());  // Speeds


        Pattern p = Pattern.compile("[\\d\\s]+");
        int cat = 0;
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                Matcher m = p.matcher(rad);
                while (m.find()) {
                    String num = m.group(0).replaceAll("\\s+", "");
                    arr.get(cat).add(Long.parseLong(num));
                }
                cat ++;
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}


        //Deklarera lista att lägga vinnartider i
        List<Long> winnerTimes = new ArrayList<>();

        //Leta igenom vilka tider som är vinnartider för varje race
        boolean goOn = true;
        Long holdOnTime = Long.parseLong("0");
        Long currDist;
        Long raceTime = arr.get(0).get(0);
        while (goOn) {
            currDist = holdOnTime*(raceTime-holdOnTime);
            //Jämför med tidigare rekord
            if (currDist > arr.get(1).get(0)) {
                winnerTimes.add(holdOnTime);
            }
            holdOnTime ++;
            if (holdOnTime.equals(raceTime)) {
                goOn = false;
            }
        }

        //Addera ihop antal vinnartider
        System.out.println("Antal vinnartider: "+winnerTimes.size());
    }
}