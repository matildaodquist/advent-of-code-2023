import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";

        //Deklarera listor att lägga input i
        List<List<Integer>> arr = new ArrayList<>();
        arr.add(new ArrayList<>());  // Times
        arr.add(new ArrayList<>());  // Speeds


        Pattern p = Pattern.compile("\\d+");
        int cat = 0;
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                Matcher m = p.matcher(rad);
                while (m.find()) {
                    String num = m.group(0).replaceAll("\\s+", "");
                    arr.get(cat).add(Integer.parseInt(num));
                }
                cat ++;
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}


        //Deklarera listor att lägga vinnartider i
        List<List<Integer>> winnerTimes = new ArrayList<>();
        for (int i = 0; i < arr.get(0).size(); ++i) {
            winnerTimes.add(new ArrayList<>());  //add list for each race [[],[],[]]
        }

        //Leta igenom vilka tider som är vinnartider för varje race
        for (int i = 0; i < arr.get(0).size(); ++i) {
            boolean goOn = true;
            int holdOnTime = 0;
            int currDist;
            int raceTime = arr.get(0).get(i);
            while (goOn) {
                currDist = holdOnTime*(raceTime-holdOnTime);
                //Jämför med tidigare rekord
                if (currDist > arr.get(1).get(i)) {
                    winnerTimes.get(i).add(holdOnTime);
                }
                holdOnTime ++;
                if (holdOnTime == raceTime) {
                    goOn = false;
                }
            }
        }

        //Ta fram siffra som beskriver marignalen av vinster multiplicerat
        int multiplicationOfMargin = 1;
        for (int i = 0; i < winnerTimes.size(); ++i) {
            multiplicationOfMargin = multiplicationOfMargin*winnerTimes.get(i).size();
        }
        System.out.println("Antal vinnartider multiplicerat: "+multiplicationOfMargin);
    }
}