import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        //Läs all input och lägg i en lista
        ArrayList<String> inputList = new ArrayList<>();
        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                inputList.add(rad);
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}
        expandUniverse(inputList);

        //Parsa om så vi har "universe" som är en lista av lista, där galaxer har specifika nummer (som strings)
        List<List<String>> universe = new ArrayList<>();
        List<String> lstOfGals = new ArrayList<>();
        int galaxyNum = 1;
        for (String lines : inputList) {
            List<String> convertLst = new ArrayList<>();
            for (int i= 0; i < lines.length(); i++) {
                String numToAdd;
                if (lines.charAt(i) == '#') {
                    numToAdd = String.valueOf(galaxyNum);
                    lstOfGals.add(numToAdd);
                    galaxyNum ++;
                } else {
                    numToAdd = String.valueOf(lines.charAt(i));
                }
                convertLst.add(numToAdd);
            }
            universe.add(convertLst);
        }

        for (List<String> lines : universe) {
            System.out.println(lines);
        }

        //Hämta vart alla galaxer finns och spara i hashmap hashGals
        HashMap<String, List<Integer>> hashGals = new HashMap<>();
        int expansionNum = 1000000;
        expansionNum --;
        hashGals = getGals(universe, expansionNum);


        //Kolla avstånd mellan alla galaxer och lägg i hashmap hashDistance
        HashMap<List<Integer>, Integer> hashDistance = new HashMap<>();
        for (String galaxy : lstOfGals) {
            for (String galaxyToPair : lstOfGals) {
                List<Integer> newPair = new ArrayList<>();
                if (!Objects.equals(galaxy, galaxyToPair)) {
                    newPair.add(Integer.valueOf(galaxy));
                    newPair.add(Integer.valueOf(galaxyToPair));
                    Collections.sort(newPair);
                    if (!(hashDistance.containsKey(newPair))) {
                        List<Integer> galaxyIdx = hashGals.get(galaxy);
                        List<Integer> galaxyToPairIdx = hashGals.get(galaxyToPair);
                        int yDist = Math.abs(galaxyIdx.get(0) - galaxyToPairIdx.get(0));
                        int xDist = Math.abs(galaxyIdx.get(1) - galaxyToPairIdx.get(1));
                        hashDistance.put(newPair, yDist + xDist);
                    }
                }
            }
        }

        long sumOfDistances = 0L;
        for (int value : hashDistance.values()) {
            sumOfDistances = sumOfDistances+value;
        }
        System.out.println("Summan av alla avstånd mellan galaxer är: "+sumOfDistances);
    }



    //Hjälpfunktion som tar fram alla galaxer och deras index och lägger i en hashmap
    private static HashMap<String, List<Integer>> getGals(List<List<String>> uni, int expNum) {
        HashMap<String, List<Integer>> hashGals = new HashMap<>();
        int rowGal = 0; //Vilken rad iterationen är på
        //Gå igenom universum och hitta en galax
        for (List<String> lines : uni) {
            int colGal = 0; //Vilken kolumn iterationen är i
            for (String gal : lines) {
                if ((!Objects.equals(gal, ".")) && (!Objects.equals(gal, "+")) &&
                        (!Objects.equals(gal, "-")) && (!Objects.equals(gal, "|"))) { //galax hittad
                    List<Integer> indexLst = new ArrayList<>();
                    indexLst.add(rowGal);
                    indexLst.add(colGal);
                    hashGals.put(gal, indexLst);
                }
                if (Objects.equals(gal, "|")) {
                    colGal = colGal + expNum;
                } else {
                    colGal ++;
                }
            }
            if (Objects.equals(lines.getFirst(), "-")) {
                rowGal = rowGal + expNum;
            } else {
                rowGal++;
            }
        }
        return hashGals;
    }

    //Hjälpfunktion som markerar var det finns extra tillkommet space i universe
    private static void expandUniverse(ArrayList<String> inputList) {
        // Lägg till space för tomma kolumner
        ArrayList<String> copyList = new ArrayList<>(inputList.size());
        copyList.addAll(inputList);
        int numAdded = 0;
        for (int i = 0; i < copyList.getFirst().length(); i++) {
            boolean noGalaxy = true;
            for (String s : copyList) {
                if (s.charAt(i) != '.') {
                    noGalaxy = false;
                    break;
                }
            }
            if (noGalaxy) {
                for (int k = 0; k < inputList.size(); k++) {
                    String str = inputList.get(k);
                    str = str.substring(0, i+numAdded) + "|" + str.substring(i+numAdded);
                    inputList.set(k, str);
                }
                numAdded ++;
            }
        }
        //Lägg till space för tomma rader
        copyList = new ArrayList<>(inputList.size());
        copyList.addAll(inputList);
        numAdded = 0;
        for (int j = 0; j < copyList.size(); j++) {
            boolean noGalaxy = true;
            for (int k = 0; k < copyList.getFirst().length(); k++) {
                if ((copyList.get(j).charAt(k) != '.') && (copyList.get(j).charAt(k) != '|')) {
                    noGalaxy = false;
                }
            }
            String newString = "";
            if (noGalaxy) {
                for (int i = 0; i < copyList.getFirst().length(); i++) {
                    if (copyList.get(j).charAt(i) == '|') {
                        newString = newString + "+";
                    } else {
                        newString = newString + "-";
                    }
                }
                inputList.add(j+numAdded, newString);
                numAdded ++;
            }
        }
    }
}