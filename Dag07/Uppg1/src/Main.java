import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";

        //Deklarera listor att lägga input i utifrån kategorier av hands
        List<List<List<String>>> typeOfHands = new ArrayList<>();
        typeOfHands.add(new ArrayList<>());  // fiveOfAKind - en typ av char
        typeOfHands.add(new ArrayList<>());  // fourOfAKind - två typer av char (varav en ensam char)
        typeOfHands.add(new ArrayList<>());  // fullHouse - två typer av char
        typeOfHands.add(new ArrayList<>());  // threeOfAKind -tre typer av char
        typeOfHands.add(new ArrayList<>());  // twoPair -tre typer av char (varav en ensam char)
        typeOfHands.add(new ArrayList<>());  // onePair fyra typer av char
        typeOfHands.add(new ArrayList<>());  // highCard fem typer av char

        //Ge möjlighet för [[[AAAAA,55555][72,95]], [[8888K,QQ9QQ][152,83]] o.s.v.]
        for (int i = 0; i < typeOfHands.size(); ++i) {
            typeOfHands.get(i).add(new ArrayList<>());  // Hands
            typeOfHands.get(i).add(new ArrayList<>());  // Bids
        }

        Pattern cardP = Pattern.compile("^[AKQJT\\d]+");
        Pattern bidP = Pattern.compile("\\d+$");
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                //Leta hands
                Matcher m = cardP.matcher(rad);
                int handsType = 0;
                while (m.find()) {
                    String cards = m.group(0).replaceAll("\\s+", "");
                    //Ta reda på vilken kategori av hands och placera i lista
                    handsType = decideTypeOfHand(cards);
                    typeOfHands.get(handsType).get(0).add(cards);
                }
                //Leta bids och placera i lista i index som tillhörande hands
                Matcher n = bidP.matcher(rad);
                while (n.find()) {
                    String cards = n.group(0).replaceAll("\\s+", "");
                    typeOfHands.get(handsType).get(1).add(cards);
                }
            }
            scanner.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Hittade inte input");
        }

        //Gå igenom alla listor med olika kategorier av hands och räkna poäng
        int score = 0;
        int rank = 1;
        for (int c = 0; c < typeOfHands.size(); c++) {
            if (typeOfHands.get(6-c).get(0).size() == 1) {
                score = score + (rank * Integer.parseInt(typeOfHands.get(6-c).get(1).get(0)));
                rank ++;
            } else if (typeOfHands.get(6-c).get(0).size() > 1) {
                List<String> sortedHands = new ArrayList<>();
                sortedHands = sortHands(typeOfHands.get(6-c).get(0));
                for (int d = 0; d < sortedHands.size(); d++) {
                    int pointIdx = typeOfHands.get(6-c).get(0).indexOf(sortedHands.get(sortedHands.size()-d-1));
                    score = score + (rank * Integer.parseInt(typeOfHands.get(6-c).get(1).get(pointIdx)));
                    rank ++;
                }
            }
        }
        System.out.println("Totala poängen blir: "+score);

    }

    // Hjälpfunktion som sorterar en kategori av hands
    private static List<String> sortHands(List<String> handsList) {
        List<String> sortedHandsList = new ArrayList<>();
        sortedHandsList.add(handsList.get(0));
        String orderString = "AKQJT98765432";
        for (int i = 1; i < handsList.size(); i++) {
            boolean added = false;
            for (int k = 0; k < handsList.size()-1; k++) {
                if (added) {break;}
                int charNum = 0;
                while (true) {
                    if (handsList.get(i).charAt(charNum) ==
                            sortedHandsList.get(k).charAt(charNum)) {
                        charNum++;
                    } else if (orderString.indexOf(handsList.get(i).charAt(charNum)) >
                            orderString.indexOf(sortedHandsList.get(k).charAt(charNum))) {
                        if (k == sortedHandsList.size()-1) {
                            sortedHandsList.add(handsList.get(i));
                            added = true;
                        }
                        break;
                    } else if (orderString.indexOf(handsList.get(i).charAt(charNum)) <
                            orderString.indexOf(sortedHandsList.get(k).charAt(charNum))) {
                        sortedHandsList.add(k, handsList.get(i));
                        added = true;
                        break;
                    }
                }
            }
        }
        return sortedHandsList;
    }

    // Hjälpfunktion som tar fram vilken kategori av hand
    private static int decideTypeOfHand(String hand) {
        HashSet<Character> char_set = new HashSet<>();

        // Använd Hashset för att se vilka olika type av chars i en string
        for (int c = 0; c < hand.length(); c++) {
            char_set.add(hand.charAt(c));
        }

        int handCat = 0;

        // Två kategorier av hands som har samma antal olika typer av chars...
        if ((char_set.size() == 2) || (char_set.size() == 3)) {
            int occurences = 0;
            for (int c = 0; c < hand.length(); c++) {
                occurences = 0;
                char charType = hand.charAt(c);
                for (int d = 0; d < hand.length(); d++) {
                    if (hand.charAt(d) == charType) {
                        occurences++;
                    }
                }
                if (occurences == 1) {
                    if (char_set.size() == 2) {
                        handCat = 1;
                    } else {
                        handCat = 4;
                    }
                }
                if (occurences == 2) {
                    if (char_set.size() == 2) {
                        handCat = 2;
                        break;
                    }
                }
                if (occurences == 3) {
                    if (char_set.size() == 3) {
                        handCat = 3;
                        break;
                    }
                }
            }
        }

        if (char_set.size() == 4) {
            handCat = 5;
        }
        if (char_set.size() == 5) {
            handCat = 6;
        }

        return handCat;
    }
}