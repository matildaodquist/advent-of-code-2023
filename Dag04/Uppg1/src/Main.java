import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";
        ArrayList<String> inputList = new ArrayList<>();
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                inputList.add(rad);
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}

        //Deklarera variabler
        int inputSize = inputList.size();
        int addedPoints = 0;

        //Gå igenom alla rader i inputen och parsa datat
        for (int i = 0; i < inputSize; i++) {
            List<List<String>> tempList = new ArrayList<>();
            tempList = parseInput(inputList.get(i));

            int points = 0;
            for (int j = 0; j < tempList.get(1).size(); j++) {
                if (tempList.get(0).contains(tempList.get(1).get(j))) {
                    if (points == 0){
                        points = 1;
                    } else {
                        points = points * 2;
                    }
                }
            }
            addedPoints = addedPoints + points;
        }

        System.out.println("Adderade numrena av vinnartalen är: "+addedPoints);

    }

    // Hjälpfunktion som parsar input och lägger i listor så man har vinnarnummer och kortnummer sorterat
    private static List<List<String>> parseInput(String line) {
        List<List<String>> arr = new ArrayList<>();
        List<String> winnersList = new ArrayList<>();
        List<String> cardsList = new ArrayList<>();

        line = line + ' ';
        int counter = 0;

        Pattern p = Pattern.compile("\\d+\\s+");
        Matcher m = p.matcher(line);

        while(m.find()) {
            String num = m.group(0).replaceAll("\\D+","");
            if (counter < 10) { //Hårdkodat antalet kort som utgör vinnarkort
                winnersList.add(num);
            } else {
                cardsList.add(num);
            }
            counter ++;
        }
        arr.add(winnersList);
        arr.add(cardsList);
        return arr;
    }
}