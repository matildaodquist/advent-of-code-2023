import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";

        List<List<Long>> arr = new ArrayList<>();
        arr.add(new ArrayList<>());  // seeds
        arr.add(new ArrayList<>());  // seedToSoil
        arr.add(new ArrayList<>());  // soilToFert
        arr.add(new ArrayList<>());  // fertToWater
        arr.add(new ArrayList<>());  // waterToLight
        arr.add(new ArrayList<>());  // lightToTemp
        arr.add(new ArrayList<>());  // tempToHumid
        arr.add(new ArrayList<>());  // humidToLoc

        Pattern p = Pattern.compile("\\d+");
        int cat = 0;
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                Matcher m = p.matcher(rad);
                if (rad.isEmpty()) {
                    cat++;
                } else {
                    while (m.find()) {
                        String num = m.group(0).replaceAll("\\s+", "");
                        Long l = Long.parseLong(num);
                        arr.get(cat).add(l);
                    }
                }
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}

        //Deklarera en lista med alla samlade destinationssiffror
        List<Long> resultList = new ArrayList<>();
        List<Long> potentialList = new ArrayList<>();

        //Ta fram vilket område av seeds som ser lovande ut
        for (int i = 0; i < arr.get(0).size(); ++i) {
            if (i % 2 == 0) {
                Long cntr = Long.valueOf("0");
                Long currSeed = arr.get(0).get(i);
                Long maxVal = currSeed+arr.get(0).get(i+1);
                while (currSeed < maxVal) {
                    currSeed = arr.get(0).get(i) + cntr;
                    Long seedNum = countSeed(currSeed, arr);
                    resultList.add(seedNum);
                    potentialList.add(arr.get(0).get(i));
                    cntr = cntr + Long.valueOf("100000");
                }
            }
        }
        Integer idx = resultList.indexOf(Collections.min(resultList));
        Integer span = arr.get(0).indexOf(potentialList.get(idx));
        Long shortestSeedNum = Long.valueOf("9999999999");

        //Gå igenom och räkna på nytt, för hela det område av seeds som ser lovande ut
        Long currSeed = potentialList.get(idx);
        Long maxVal = currSeed+arr.get(0).get(span+1);
        while (currSeed < maxVal) {
            Long seedNum = countSeed(currSeed, arr);
            System.out.println("Hittat nr: "+seedNum+" Rekord: "+shortestSeedNum+" Ursprungsseed: "+potentialList.get(idx)+" Nuvarandeseed: "+currSeed);
            if (seedNum < shortestSeedNum) {
                shortestSeedNum = seedNum;
            }
            currSeed++;
        }
        System.out.println("Det bästa fröet ger avstånd: "+shortestSeedNum);
    }

    // Hjälpfunktion som gör beräkningar för seed
    private static Long countSeed(Long seed, List<List<Long>> arr) {
            Long currDest = seed;
            //Låt varje seed gå igenom varje planterigsregel
            for (int j = 1; j < arr.size(); ++j) {
                int itr = 0;
                while(true) {
                    if (((currDest <= arr.get(j).get(1+itr*3)+arr.get(j).get(2+itr*3)-1))
                            && (currDest >= arr.get(j).get(1+itr*3))) {
                        //Hittat match, flytta till ny destination
                        currDest = arr.get(j).get(0+itr*3)+(currDest-arr.get(j).get(1+itr*3));
                        break;
                    } else {
                        itr = itr+1;
                        if (itr == arr.get(j).size()/3) {
                            //Hittade ingen match, flytta inte till ny destination
                            break;
                        }
                    }
                }
            }
            return currDest;
    }

}