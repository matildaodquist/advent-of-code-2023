import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";

        List<List<Long>> arr = new ArrayList<>();
        arr.add(new ArrayList<>());  // seeds
        arr.add(new ArrayList<>());  // seedToSoil
        arr.add(new ArrayList<>());  // soilToFert
        arr.add(new ArrayList<>());  // fertToWater
        arr.add(new ArrayList<>());  // waterToLight
        arr.add(new ArrayList<>());  // lightToTemp
        arr.add(new ArrayList<>());  // tempToHumid
        arr.add(new ArrayList<>());  // humidToLoc

        Pattern p = Pattern.compile("\\d+");
        int cat = 0;
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                Matcher m = p.matcher(rad);
                if (rad.isEmpty()) {
                    cat++;
                } else {
                    while (m.find()) {
                        String num = m.group(0).replaceAll("\\s+", "");
                        Long l = Long.parseLong(num);
                        arr.get(cat).add(l);
                    }
                }
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}

        //Deklarera en lista med alla samlade destinationssiffror
        List<Long> destList = new ArrayList<>();


        //Gör beräkningar för ett seed i taget
        for (int i = 0; i < arr.get(0).size(); ++i) {
            Long currDest = arr.get(0).get(i);
            //Låt varje seed gå igenom varje planterigsregel
            for (int j = 1; j < arr.size(); ++j) {
                int itr = 0;
                while(true) {
                    if (((currDest <= arr.get(j).get(1+itr*3)+arr.get(j).get(2+itr*3)-1))
                            && (currDest >= arr.get(j).get(1+itr*3))) {
                        //Hittat match, flytta till ny destination
                        currDest = arr.get(j).get(0+itr*3)+(currDest-arr.get(j).get(1+itr*3));
                        break;
                    } else {
                        itr = itr+1;
                        if (itr == arr.get(j).size()/3) {
                            //Hittade ingen match, flytta inte till ny destination
                            break;
                        }
                    }
                }
            }
            destList.add(currDest);
        }

        Collections.sort(destList);
        System.out.println("Minsta destinationen är: "+destList.get(0));
    }
}