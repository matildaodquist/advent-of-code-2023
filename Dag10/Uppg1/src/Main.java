import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Main {
    static int yPos = 0;
    static int xPos = 0;
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";
        List<List<Character>> inputList = new ArrayList<>();
        //Deklarera variabler
        int itr = 0;
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                inputList.add(new ArrayList<>());
                String rad = scanner.nextLine();
                int itx = 0;
                for (char ch : rad.toCharArray()) {
                    inputList.get(itr).add(ch);
                    if (ch == 'S') {
                        yPos = itr;
                        xPos = itx;
                    }
                    itx ++;
                }
                itr++;
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}


        //Deklarera variabler
        String origin = "None";
        int steps = 0;
        int xPosOriginal = xPos;
        int yPosOriginal = yPos;


        //Stega igenom kartan och räkna steg
        while (true) {
            origin = goStep(inputList, origin);
            steps ++;
            if ((xPosOriginal == xPos) && (yPosOriginal == yPos)) {
                break;
            }
        }

        System.out.println("Antal steg för att vara halvvägs i loopen är: "+steps/2);
    }


    // Hjälpfunktion som kollar på chars och går steg i vägkartan
    private static String goStep(List<List<Character>> map, String origin) {
        switch(map.get(yPos).get(xPos)) {
            case 'S':
                if (xPos != 0) {
                    if ((map.get(yPos).get(xPos-1) == '-') || (map.get(yPos).get(xPos-1) == 'F') ||
                            (map.get(yPos).get(xPos-1) == 'L')) {
                        xPos--;
                        origin = "right";
                        break;
                    }
                }
                if (xPos < map.get(yPos).size()-1) {
                    if ((map.get(yPos).get(xPos+1) == '-') || (map.get(yPos).get(xPos+1) == '7') ||
                            (map.get(yPos).get(xPos+1) == 'J')) {
                        xPos++;
                        origin = "left";
                        break;
                    }
                }
                if (yPos != 0) {
                    if ((map.get(yPos-1).get(xPos) == '|') || (map.get(yPos-1).get(xPos) == '7') ||
                            (map.get(yPos-1).get(xPos) == 'F')) {
                        yPos--;
                        origin = "down";
                        break;
                    }
                }
                if (yPos < map.size()-1) {
                    if ((map.get(yPos+1).get(xPos) == '|') || (map.get(yPos+1).get(xPos) == 'L') ||
                            (map.get(yPos+1).get(xPos) == 'J')) {
                        yPos++;
                        origin = "up";
                        break;
                    }
                }
            case '|':
                if (Objects.equals(origin, "up")) {
                    yPos ++;
                } else {
                    yPos --;
                }
                break;
            case '-':
                if (Objects.equals(origin, "left")) {
                    xPos ++;
                } else {
                    xPos --;
                }
                break;
            case 'L':
                if (Objects.equals(origin, "up")) {
                    xPos ++;
                    origin = "left";
                } else {
                    yPos --;
                    origin = "down";
                }
                // code block
                break;
            case 'J':
                if (Objects.equals(origin, "up")) {
                    xPos --;
                    origin = "right";
                } else {
                    yPos --;
                    origin = "down";
                }
                break;
            case '7':
                if (Objects.equals(origin, "left")) {
                    yPos ++;
                    origin = "up";
                } else {
                    xPos --;
                    origin = "right";
                }
                break;
            case 'F':
                if (Objects.equals(origin, "right")) {
                    yPos ++;
                    origin = "up";
                } else {
                    xPos ++;
                    origin = "left";
                }
                break;
        }
        return origin;
    }

}
