import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class Main {
    static int yPos = 0;
    static int xPos = 0;
    static String origin = "None";
    static String currDir = "None";
    static int startDirToAvoid;
    static boolean changeDir;

    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        //String filepath = "../exInput2.txt";
        //String filepath = "../exInput3.txt";
        //String filepath = "../exInput4.txt";
        String filepath = "../Input.txt";
        List<List<Character>> inputList = new ArrayList<>();
        List<List<Character>> oldInputList = new ArrayList<>();
        List<List<Character>> pipesToKeepList = new ArrayList<>();
        //Deklarera variabler
        int itr = 0;
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                inputList.add(new ArrayList<>());
                oldInputList.add(new ArrayList<>());
                pipesToKeepList.add(new ArrayList<>());
                String rad = scanner.nextLine();
                int itx = 0;
                for (char ch : rad.toCharArray()) {
                    inputList.get(itr).add(ch);
                    oldInputList.get(itr).add(ch);
                    pipesToKeepList.get(itr).add('.');
                    if (ch == 'S') {
                        yPos = itr;
                        xPos = itx;
                    }
                    itx ++;
                }
                itr++;
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}


       //Skriv ut hur kartan ser ut från början
        for (int i = 0; i < inputList.size(); ++i) {
            System.out.println(inputList.get(i));
        }

        //Deklarera variabler
        int xPosOriginal = xPos;
        int yPosOriginal = yPos;


       //Stega igenom kartan och omvandla 'tiles lying around' till punkter
        while (true) {
            goStep(inputList);
            pipesToKeepList.get(yPos).set(xPos, 'K');
            if ((xPosOriginal == xPos) && (yPosOriginal == yPos)) {
                break;
            }
        }
        for (int i = 0; i < inputList.size(); ++i) {
            for (int j = 0; j < inputList.get(i).size(); ++j) {
                if (pipesToKeepList.get(i).get(j) != 'K') {
                    inputList.get(i).set(j, '.');
                    oldInputList.get(i).set(j, '.');
                }
            }
        }

        //Återställ variabler
        changeDir = false;
        xPos = xPosOriginal;
        yPos = yPosOriginal;


        //Stega igenom kartan igen, bestäm vägen, och markera ut punkter på en sida av loopen
        while (true) {
            //Gå steg ett i taget i kartan
            goStep(inputList);
            //Lägg till '0' för att markera vilka punkter som är inne i loopen
            inputList = addZeros(inputList);
            if (((xPosOriginal == xPos) && (yPosOriginal == yPos)) || (changeDir)) {
                break;
            }
        }

        //Om fel riktning togs från början så 0 hamnade utanför loopen börja då om igen men ta andra riktningen
        if (changeDir) {
            inputList = new ArrayList<>(oldInputList);
            xPos = xPosOriginal;
            yPos = yPosOriginal;
            while (true) {
                //Gå steg ett i taget i kartan
                goStep(inputList);
                //Lägg till '0' för att markera vilka punkter som är inne i loopen
                inputList = addZeros(inputList);
                if ((xPosOriginal == xPos) && (yPosOriginal == yPos)) {
                    break;
                }
            }
        }

        //Skriv ut hur kartan ser ut när markeringar är gjorda
        System.out.println("");
        for (int i = 0; i < inputList.size(); ++i) {
            System.out.println(inputList.get(i));
        }

        //Räkna "tiles enclosed by the loop" och skriv ut svaret
        int numTiles = 0;
        for (int i = 0; i < inputList.size(); ++i) {
            for (int j = 0; j < inputList.get(i).size(); ++j) {
                if (inputList.get(i).get(j) == '0') {
                    numTiles ++;
                }
            }
        }
        System.out.println("");
        System.out.println("Antal 'tiles enclosed by the loop' är: "+numTiles);
    }


    //Hjälpfunktion som markerar med '0' vilka punkter som är innanför loopen, tar hjälp av addZerosDir
    private static List<List<Character>> addZeros(List<List<Character>> inputList) {
        Character currPos = inputList.get(yPos).get(xPos);
        switch(currDir) {
            case "down":
                //Kolla höger
                if ((currPos == '|') || (currPos == 'J') || (currPos == 'L')) {
                    inputList = addZerosDir(inputList, "right");
                }
                //Kolla under
                if (currPos == 'J') {
                    inputList = addZerosDir(inputList, "down");
                }
                //Kolla över
                if (currPos == 'L') {
                    inputList = addZerosDir(inputList, "up");
                }
                break;
            case "up":
                //Kolla vänster
                if ((currPos == '|') || (currPos == 'F') || (currPos == '7')) {
                    inputList = addZerosDir(inputList, "left");
                }
                //Kolla över
                if (currPos == 'F') {
                    inputList = addZerosDir(inputList, "up");
                }
                //Kolla under
                if (currPos == '7'){
                    inputList = addZerosDir(inputList, "down");
                }
                break;
            case "left":
                //Kolla under
                if ((currPos == '-') || (currPos == 'L') || (currPos == 'F')){
                    inputList = addZerosDir(inputList, "down");
                }
                //Kolla vänster
                if (currPos == 'L') {
                    inputList = addZerosDir(inputList, "left");
                }
                //Kolla höger
                if (currPos == 'F') {
                    inputList = addZerosDir(inputList, "right");
                }
                break;
            case "right":
                //Kolla över
                if ((currPos == '-') || (currPos == '7') || (currPos == 'J')) {
                    inputList = addZerosDir(inputList, "up");
                }
                //Kolla höger
                if (currPos == '7') {
                    inputList = addZerosDir(inputList, "right");
                }
                //Kolla vänster
                if (currPos == 'J') {
                    inputList = addZerosDir(inputList, "left");
                }
                break;
        }
        return inputList;
    }


    //Hjälpfunktion till addZeros, där man skickar in en riktning och lägger till '0'
    private static List<List<Character>> addZerosDir(List<List<Character>> inputList, String dir) {
        boolean goOn = true;
        int its = 1;
        switch (dir) {
            case "left":
                while (goOn) {
                    if (xPos - its > -1) {
                        if ((inputList.get(yPos).get(xPos - its) == '.') ||
                                (inputList.get(yPos).get(xPos - its) == '0')) {
                            inputList.get(yPos).set(xPos - its, '0');
                            if (xPos - its == 0) {
                                changeDir = true;
                            }
                        } else {
                            goOn = false;
                        }
                    } else {
                        goOn = false;
                    }
                    its ++;
                }
                break;
            case "right":
                while (goOn) {
                    if (xPos + its < inputList.get(yPos).size()) {
                        if ((inputList.get(yPos).get(xPos + its) == '.') ||
                                (inputList.get(yPos).get(xPos + its) == '0')) {
                            inputList.get(yPos).set(xPos + its, '0');
                            if (xPos + its == inputList.get(yPos).size()-1) {
                                changeDir = true;
                            }
                        } else {
                            goOn = false;
                        }
                    } else {
                        goOn = false;
                    }
                    its++;
                }
                break;
            case "up":
                while (goOn) {
                    if (yPos - its > -1) {
                        if ((inputList.get(yPos - its).get(xPos) == '.') ||
                                (inputList.get(yPos - its).get(xPos) == '0')) {
                            inputList.get(yPos - its).set(xPos, '0');
                            if (yPos - its == 0) {
                                changeDir = true;
                            }
                        } else {
                            goOn = false;
                        }
                    } else {
                        goOn = false;
                    }
                    its++;
                }
                break;
            case "down":
                while (goOn) {
                    if (yPos + its < inputList.size()) {
                        if ((inputList.get(yPos + its).get(xPos) == '.') ||
                                (inputList.get(yPos + its).get(xPos) == '0')){
                            inputList.get(yPos + its).set(xPos, '0');
                            if (yPos + its == inputList.size()-1) {
                                changeDir = true;
                            }
                        } else {
                            goOn = false;
                        }
                    } else {
                        goOn = false;
                    }
                    its ++;
                }
                break;
        }
        return inputList;
    }


    // Hjälpfunktion som kollar på chars och går steg i vägkartan
    private static void goStep(List<List<Character>> map) {
        switch(map.get(yPos).get(xPos)) {
            case 'S':
                if ((xPos != 0) && (startDirToAvoid != 0)){
                    if ((map.get(yPos).get(xPos-1) == '-') || (map.get(yPos).get(xPos-1) == 'F') ||
                            (map.get(yPos).get(xPos-1) == 'L')) {
                        xPos--;
                        origin = "right";
                        currDir = "left";
                        startDirToAvoid = 0;
                        break;
                    }
                }
                if ((xPos < map.get(yPos).size()-1) && (startDirToAvoid != 1)){
                    if ((map.get(yPos).get(xPos+1) == '-') || (map.get(yPos).get(xPos+1) == '7') ||
                            (map.get(yPos).get(xPos+1) == 'J')) {
                        xPos++;
                        origin = "left";
                        currDir = "right";
                        startDirToAvoid = 1;
                        break;
                    }
                }
                if ((yPos != 0) && (startDirToAvoid != 2)) {
                    if ((map.get(yPos-1).get(xPos) == '|') || (map.get(yPos-1).get(xPos) == '7') ||
                            (map.get(yPos-1).get(xPos) == 'F')) {
                        yPos--;
                        origin = "down";
                        currDir = "up";
                        startDirToAvoid = 2;
                        break;
                    }
                }
                if ((yPos < map.size()-1) && (startDirToAvoid != 3)) {
                    if ((map.get(yPos+1).get(xPos) == '|') || (map.get(yPos+1).get(xPos) == 'L') ||
                            (map.get(yPos+1).get(xPos) == 'J')) {
                        yPos++;
                        origin = "up";
                        currDir = "down";
                        startDirToAvoid = 3;
                        break;
                    }
                }
            case '|':
                if (Objects.equals(origin, "up")) {
                    yPos ++;
                } else {
                    yPos --;
                }
                break;
            case '-':
                if (Objects.equals(origin, "left")) {
                    xPos ++;
                } else {
                    xPos --;
                }
                break;
            case 'L':
                if (Objects.equals(origin, "up")) {
                    xPos ++;
                    origin = "left";
                    currDir = "right";
                } else {
                    yPos --;
                    origin = "down";
                    currDir = "up";
                }
                // code block
                break;
            case 'J':
                if (Objects.equals(origin, "up")) {
                    xPos --;
                    origin = "right";
                    currDir = "left";
                } else {
                    yPos --;
                    origin = "down";
                    currDir = "up";
                }
                break;
            case '7':
                if (Objects.equals(origin, "left")) {
                    yPos ++;
                    origin = "up";
                    currDir = "down";
                } else {
                    xPos --;
                    origin = "right";
                    currDir = "left";
                }
                break;
            case 'F':
                if (Objects.equals(origin, "right")) {
                    yPos ++;
                    origin = "up";
                    currDir = "down";
                } else {
                    xPos ++;
                    origin = "left";
                    currDir = "right";
                }
                break;
        }
    }

}