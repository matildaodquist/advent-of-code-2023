import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";
        ArrayList<String> inputList = new ArrayList<>();
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                inputList.add(rad);
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}

        //Deklarera variabler
        int inputSize = inputList.size();
        int outputNum = 0;

        //Gå igenom alla rader och hämta siffrorna och addera dem till variabel outputNum
        for (int i = 0; i < inputSize; ++i) {
            String currLineF = inputList.get(i);

            currLineF = currLineF.replaceAll("one","o1e");
            currLineF = currLineF.replaceAll("two","t2o");
            currLineF = currLineF.replaceAll("three","t3e");
            currLineF = currLineF.replaceAll("four","f4");
            currLineF = currLineF.replaceAll("five","f5e");
            currLineF = currLineF.replaceAll("six","s6");
            currLineF = currLineF.replaceAll("seven","s7n");
            currLineF = currLineF.replaceAll("eight","e8t");
            currLineF = currLineF.replaceAll("nine","n9e");
            currLineF = currLineF.replaceAll("\\D+","");
            
            int lineLength =currLineF.length();
            String first = currLineF.substring(0,1);
            String last = currLineF.substring(lineLength-1,lineLength);

            outputNum = outputNum+Integer.parseInt(first+last);
        }
        System.out.println("Adderade numret är: " + outputNum);
    }
}