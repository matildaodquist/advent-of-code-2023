import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista
        List<List<Integer>> inputList = new ArrayList<>();

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";

        Pattern p = Pattern.compile("\\d+|-\\d+");
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            int lineNum = 0;
            while (scanner.hasNextLine()) {
                inputList.add(new ArrayList<>());
                String rad = scanner.nextLine();
                Matcher m = p.matcher(rad);
                while (m.find()) {
                    String newNum = m.group(0).replaceAll("\\s+", "");
                    inputList.get(lineNum).add(Integer.parseInt(newNum));
                }
                lineNum++;
            }
            scanner.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Hittade inte input");
        }


        //Deklarera useful variabler
        List<List<List<Integer>>> allPredictionLsts = new ArrayList<>();

        for (int i = 0; i < inputList.size(); ++i) {
            List<List<Integer>> predictionLst = new ArrayList<>();
            predictionLst.add(inputList.get(i));
            int itr = 0;
            while (true) {
                boolean allZero = true;
                predictionLst.add(new ArrayList<>());
                for (int j = 0; j < predictionLst.get(itr).size()-1; ++j) {
                    int diff = predictionLst.get(itr).get(j+1)-predictionLst.get(itr).get(j);
                    predictionLst.get(itr+1).add(diff);
                    if (diff != 0) {
                        allZero = false;
                    }
                }
                if (allZero) {
                    break;
                }
                itr ++;
            }
            allPredictionLsts.add(predictionLst);
        }

        //Lägg till nya predicitons-värden och lägg till i en variabel som sparar summan av dem för output
        int predicitionsAdded = 0;
        for (int i = 0; i < allPredictionLsts.size(); ++i) {
            List<List<Integer>> extrapolatedPredictionList;
            extrapolatedPredictionList = decidePredictions(allPredictionLsts.get(i));
            allPredictionLsts.set(i, extrapolatedPredictionList);
            predicitionsAdded = predicitionsAdded + allPredictionLsts.get(i).getFirst().getFirst();
        }

        System.out.println("Summan av alla predictions blir: "+predicitionsAdded);
    }

    // Hjälpfunktion som går igenom alla talföljder och lägger till "prediction" i dem
    private static List<List<Integer>> decidePredictions(List<List<Integer>> numProg) {
        for (int i = 0; i < numProg.size(); ++i) {
            if (i == 0) {
                numProg.get(numProg.size()-i-1).add(0);
            } else {
                int numToRemove = numProg.get(numProg.size()-i).getFirst();
                int numToAdd = numProg.get(numProg.size()-i-1).getFirst();
                numProg.get(numProg.size()-i-1).add(0, numToAdd-numToRemove);
            }
        }
        return numProg;
    }

}