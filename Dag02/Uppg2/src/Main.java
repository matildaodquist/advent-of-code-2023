import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";
        ArrayList<String> inputList = new ArrayList<>();
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                inputList.add(rad);
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}

        //Deklarera variabler
        int inputSize = inputList.size();
        int minCubesPowerAddition = 0;

        //Gå igenom alla rader
        for (int i = 0; i < inputSize; i++) {
            String currLine = inputList.get(i);
            int lowestRed = countNumGame("red", currLine);
            int lowestBlue = countNumGame("blue", currLine);
            int lowestGreen = countNumGame("green", currLine);

            minCubesPowerAddition = minCubesPowerAddition + (lowestRed*lowestBlue*lowestGreen);
        }

        System.out.println("Adderad Power av samtliga spel: " + minCubesPowerAddition);
    }

    // Hjälpfunktion som tar fram vilken som är minsta antal kuber i respektive färg i ett spel
    private static int countNumGame(String color, String line) {
        ArrayList<Integer> amountList = new ArrayList<>();
        String occurence = "";
        int lowestNum = 0;

        Pattern pattern = Pattern.compile("\\d+ "+color);
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            occurence = matcher.group(0);
            occurence = occurence.replaceAll("\\D+","");
            if (Integer.parseInt(occurence) > lowestNum) {
                lowestNum = Integer.parseInt(occurence);
            }
        }
        return lowestNum;
    }
}
