import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        String filepath = "../Input.txt";
        ArrayList<String> inputList = new ArrayList<>();
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            while (scanner.hasNextLine()) {
                String rad = scanner.nextLine();
                inputList.add(rad);
            }
            scanner.close();
        }
        catch (FileNotFoundException ex) {System.out.println("Hittade inte input");}

        //Deklarera variabler
        int inputSize = inputList.size();
        int numRed = 12;
        int numBlue = 14;
        int numGreen = 13;
        int passedGamesAmount = 0;

        //Gå igenom alla rader och verifiera om antalet kuber är inom gränsen för respektive färg
        for (int i = 0; i < inputSize; i++) {
            String currLine = inputList.get(i);
            if (declareGame("red", currLine, numRed) && declareGame("blue", currLine, numBlue) &&
                    declareGame("green", currLine, numGreen)) {
                passedGamesAmount = passedGamesAmount + i+1;
            }
        }

        System.out.println("Adderade godkända spel: " + passedGamesAmount);
    }

    // Hjälpfunktion som deklarerar om ett spel överträder antal kuber
    private static boolean declareGame(String color, String line, Integer limit) {
            ArrayList<Integer> amountList = new ArrayList<>();
            String occurence = "";

            Pattern pattern = Pattern.compile("\\d+ "+color);
            Matcher matcher = pattern.matcher(line);
            while (matcher.find()) {
                occurence = matcher.group(0);
                occurence = occurence.replaceAll("\\D+","");
                if (Integer.parseInt(occurence) > limit) {
                    return false;
                }
            }
            return true;
        }
}