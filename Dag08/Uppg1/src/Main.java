import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput.txt";
        //String filepath = "../exInput2.txt";
        String filepath = "../Input.txt";

        //Deklarera listor att lägga input i utifrån kategorier av hands
        List<List<String>> posMap = new ArrayList<>();
        posMap.add(new ArrayList<>());  // position
        posMap.add(new ArrayList<>());  // next position left
        posMap.add(new ArrayList<>());  // next position right

        //Deklarera en string som är vad mönstret blir
        String patternMap = "";

        Pattern alphaP = Pattern.compile("\\w+");
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            boolean firstLn = true;
            while (scanner.hasNextLine()) {
                int itNum = 0;
                String rad = scanner.nextLine();
                Matcher m = alphaP.matcher(rad);
                while (m.find()) {
                    if (firstLn) {
                        patternMap = m.group(0).replaceAll("\\s+", "");
                        firstLn = false;
                        break;
                    }
                    posMap.get(itNum).add(m.group(0).replaceAll("\\s+", ""));
                    itNum ++;

                }
            }
            scanner.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Hittade inte input");
        }


        //Deklarera useful variabler
        boolean goOn = true;
        int currPatternPos = 0;
        int currPos = posMap.get(0).indexOf("AAA");
        int lr = 1;
        int steps = 0;


        //Gå igenom raderna och hoppa till nya platser tills ZZZ nåtts
        while (goOn) {
            if (currPatternPos > patternMap.length()-1) {
                currPatternPos = 0;
            }
            if (patternMap.charAt(currPatternPos) == 'L') {
                lr = 1;
            } else if (patternMap.charAt(currPatternPos) == 'R') {
                lr = 2;
            }

            currPos = posMap.get(0).indexOf(posMap.get(lr).get(currPos));
            currPatternPos ++;
            steps ++;
            if (Objects.equals(posMap.get(0).get(currPos), "ZZZ")) {
                goOn = false;
            }
        }

        System.out.println("Antal steg: "+steps);

    }


}