import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Main {
    public static void main(String[] args) {

        //Läs all input och lägg i en lista

        //String filepath = "../exInput3.txt";
        String filepath = "../Input.txt";

        //Deklarera listor att lägga input i utifrån kategorier av hands
        List<List<String>> posMap = new ArrayList<>();
        posMap.add(new ArrayList<>());  // position
        posMap.add(new ArrayList<>());  // next position left
        posMap.add(new ArrayList<>());  // next position right

        HashMap<String, Integer> posNums = new HashMap<>();

        //Deklarera en string som är vad mönstret blir
        String patternMap = "";

        Pattern alphaP = Pattern.compile("\\w+");
        try {
            File input = new File(filepath);
            Scanner scanner = new Scanner(input);
            int lineNum = 0;
            while (scanner.hasNextLine()) {
                int itNum = 0;
                String rad = scanner.nextLine();
                Matcher m = alphaP.matcher(rad);
                while (m.find()) {
                    if (lineNum == 0) {
                        patternMap = m.group(0).replaceAll("\\s+", "");
                        lineNum++;
                        break;
                    }
                    String newString = m.group(0).replaceAll("\\s+", "");
                    posMap.get(itNum).add(newString);
                    if (itNum == 0) {
                        posNums.put(newString, lineNum);
                        lineNum++;
                    }
                    itNum++;

                }
            }
            scanner.close();
        } catch (FileNotFoundException ex) {
            System.out.println("Hittade inte input");
        }


        //Deklarera useful variabler
        int currPatternPos = 0;
        int lr = 1;
        int steps = 0;
        List<Integer> currPosLst = new ArrayList<>();
        for (int i = 0; i < posMap.get(0).size(); ++i) {
            if (posMap.get(0).get(i).charAt(2) == 'A') {
                currPosLst.add(posMap.get(0).indexOf(posMap.get(0).get(i)));
            }
        }
        List<Integer> lcmLst = new ArrayList<>(); //lista för att hitta "least common multiple"

        for (int i = 0; i < currPosLst.size(); ++i) {
            currPatternPos = 0;
            steps = 0;
            //Sätt nya positioner för alla noder, samt kolla om alla nya positioner slutar med Z
            while (true) {
                if (currPatternPos > patternMap.length() - 1) {
                    currPatternPos = 0;
                }
                if (patternMap.charAt(currPatternPos) == 'L') {
                    lr = 1;
                } else if (patternMap.charAt(currPatternPos) == 'R') {
                    lr = 2;
                }
                int newPosIdx = posNums.get(posMap.get(lr).get(currPosLst.get(i))) - 1;
                currPosLst.set(i, newPosIdx);
                steps++;
                currPatternPos++;
                if (posMap.get(0).get(currPosLst.get(i)).charAt(2) == 'Z') {
                    lcmLst.add(steps);
                    break;
                }
            }
        }

        Long lcmMultiple = Long.valueOf(Collections.max(lcmLst)); //Deklarera variabel att sätta största int i i lcmLst
        Long lcm = lcmMultiple; //Deklarera variabel att spara storleken på ett steg i

        while (true) {
            lcm = lcm+lcmMultiple;
            boolean done = true;
            for (int i = 0; i < lcmLst.size(); ++i) {
                if (lcm % lcmLst.get(i) != 0) {
                    done = false;
                }
            }
            if (done) {
                break;
            }
            System.out.println("Nuvarande steps: " + lcm);
        }
        System.out.println("Antal steps innan alla noder når mål samtidigt: " + lcm);

    }
}
